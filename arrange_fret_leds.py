import pcbnew;
import math;
import pprint;

import os

class SimplePlugin(pcbnew.ActionPlugin):
    def round_to_next(self, val, step):
        return val - (val % step) + (step if val % step != 0 else 0)
    
    SCALE = 1000000
    def defaults(self):
        self.name = "Fret LED Arranger"
        self.category = "Modify PCB"
        self.description = "Calculates the position of the fret leds and arranges them in a grid"
        self.show_toolbar_button = False # Optional, defaults to False
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'simple_plugin.png') # Optional, defaults to ""

    def Run(self):
        # Loop throug all leds on the board starting with letter D
        # place in grid with equal spacing and offset with formula (L0 - (L0*1/(c^n))) where n is the column number
        # L0 is avaribale with default value 25.5
        # c is 2^(1/12)
        # n is the number of the column
        # each column is 6 leds
        # and 22 rows
        # the first column is offset by 1/2 the spacing

        # get the board
        board = pcbnew.GetBoard()

        s = board.GetDesignSettings()
        # get board drill origin
        origin = s.GetAuxOrigin()

        # get the led footprints
        properties = board.GetProperties()

        L0 = 64.77
        c = math.pow(2, 1/12)
        print(properties)

        leds = [f for f in board.GetFootprints() if f.GetReference().startswith("D")]
        resistors = [f for f in board.GetFootprints() if f.GetReference().startswith("R")]

        leds.sort(key=lambda x: int(x.GetReference()[1:]))
        resistors.sort(key=lambda x: int(x.GetReference()[1:]))

        fret_length = (L0 - (L0*1/math.pow(c, 23)))*10
        Total_Fret_Length = fret_length
        led_num = 0
        RowheightAdjustment = -1

        startHeight = 49.277
        endheight = 36.914
        length = 466.5
        col=1

        heightDiffrence = (startHeight-endheight)/2
        AlphaAngle = math.atan(heightDiffrence/length)

        print("AlphaAngle", AlphaAngle)
        print("fret_length", fret_length)
        print("heightDiffrence", heightDiffrence)
        print("lengt", length)

        boardStartHeight = startHeight + (2.877*2)
        boardEndHeight = endheight + (2.117*2)
        board1Point = pcbnew.VECTOR2I_MM(0, (startHeight-boardStartHeight)/2)+origin
        board2Point = board1Point+pcbnew.VECTOR2I_MM(length, heightDiffrence)
        board3Point = board2Point+pcbnew.VECTOR2I_MM(0, boardEndHeight)
        board4Point = board3Point+pcbnew.VECTOR2I_MM(-length, heightDiffrence)
        print("board1Point", board1Point-origin)
        print("board2Point", board2Point-origin)
        print("board3Point", board3Point-origin)
        print("board4Point", board4Point-origin)


        # Add center dimensions on corner of fretboard
        # get the dimension layer
        dim_layer = board.GetLayerID("Eco1.User")

        Fret_layer = board.GetLayerID("Eco2.User")
        edge_cutout_layer = board.GetLayerID("Edge.Cuts")

        #print each stiring on the board for debugging
        for stringNr in range(0,6):
            StartSpacing = startHeight/5
            EndSpacing = endheight/5
            ystart = StartSpacing*stringNr
            yend = EndSpacing*stringNr
            string = pcbnew.PCB_SHAPE(board, pcbnew.PCB_SHAPE_T, pcbnew.SHAPE_T_SEGMENT)
            string.SetStart(origin+pcbnew.VECTOR2I_MM(0, ystart))
            string.SetEnd((origin+pcbnew.VECTOR2I_MM(length, yend+heightDiffrence)))
            string.SetWidth(int(0.25 * self.SCALE))
            string.SetLayer(board.GetLayerID("User.2"))
            board.Add(string)
        
        #draw fret size for reference
        #self.Fret_Border_draw(board, board1Point, board2Point, board3Point, board4Point, dim_layer)

        #draw edge cutout
        #self.Pcb_Border_cutout(board, board1Point, board2Point, board3Point, board4Point, edge_cutout_layer)


        # loop through the led
        for fretnr in reversed(range(1,23)):
            print("fretnr", fretnr)
            print("col", col)
            # get the x position
            new_fret_length = (L0 - (L0*1/math.pow(c, fretnr)))
            new_fret_length *= 10

            x = Total_Fret_Length - new_fret_length

            # get the Length offset
            offset = (fret_length-new_fret_length)/2
            led_pos = x+offset
            print("x",x,"last x",fret_length, "ledpos", led_pos, "offset", offset)
            fret_length = new_fret_length
            # Get the current height from string to string
            yoffset = math.tan(AlphaAngle)*led_pos
            Rowheight = startHeight - yoffset*2
            #Adjust the rowheight 1mm shorter to make room for traces
            Rowheight += RowheightAdjustment

            print("startHeight", startHeight, "yoffset", yoffset, "rowheight", Rowheight)

            # Evenly distribute the leds and resistores on the fret bound by rowheight
            # 1/5 of the rowheight is the spacing between the leds
            # set the resistor positon 1,5mm blow the led except for the last row
            # the last row is to the right of the led rotated 270 degrees

            for row in range(0,6):
                # get the led number
                led = leds[led_num]
                resistor = resistors[led_num]
                led_num += 1
                
                reversedCol = col % 2 == 0
                # get the y position by dividing the rowheight by 5
                # and centering the led in the row
                led_offset = Rowheight/5
                y = led_offset*row

                # Even rows are counted from the bottom
                # this is so that the data paths contnue with minimal distance
                # between the pads
                if reversedCol:
                    y = Rowheight - y
                y += yoffset - RowheightAdjustment/2


                print("Lednr col row", led.GetReference(), col, row, y)
                # set the position
                led.SetPosition(origin+pcbnew.VECTOR2I_MM(led_pos, y))
                resistor.SetPosition(origin+pcbnew.VECTOR2I_MM(led_pos, y-1.25))
                LastRow = 0 if reversedCol else 5 
                PositionSigne = 1 if reversedCol else -1
                resistorePositon = 1.25*PositionSigne
                Gridrounding = 0.25 * PositionSigne
                if not reversedCol:
                    led.SetOrientationDegrees(90)
                    resistor.SetOrientationDegrees(90)
                else:
                    led.SetOrientationDegrees(-90)
                    resistor.SetOrientationDegrees(-90)


                if row != LastRow:
                    resistor.SetPosition(origin+pcbnew.VECTOR2I_MM(self.round_to_next(led_pos+resistorePositon,Gridrounding),y))
                else:
                    resistor.SetPosition(origin+pcbnew.VECTOR2I_MM(self.round_to_next(led_pos+resistorePositon,Gridrounding),y-2))
                    if not reversedCol:
                        resistor.SetOrientationDegrees(-90)

            #print fret helpers
            self.Draw_fret_helpers(board, origin, fret_length, startHeight, AlphaAngle, Fret_layer, x)

            # get the x position

            self.DrawInnerCutouts(board, origin, L0, c, fret_length, Total_Fret_Length, edge_cutout_layer, fretnr, led_pos, yoffset, Rowheight)
            col += 1
        # redraw the screen
        pcbnew.Refresh()

    def DrawInnerCutouts(self, board, origin, L0, c, fret_length, Total_Fret_Length, edge_cutout_layer, fretnr, led_pos, yoffset, Rowheight):
        Next_fret_length = (L0 - (L0*1/math.pow(c, fretnr-1)))
        Next_fret_length *= 10

        NextX = Total_Fret_Length - Next_fret_length
            # get the Length offset
        NEXToffset = (fret_length-Next_fret_length)/2
        NEXTled_pos = NextX+NEXToffset
        print("NextX",NextX,"fret_length",fret_length, "NEXTled_pos", NEXTled_pos, "NEXToffset", NEXToffset)

            #add edge.cutout between leds
            #InnerCut = pcbnew.ZONE(board)

        UpperPoints = (
                origin+pcbnew.VECTOR2I_MM(led_pos+2.5, 2+yoffset),
                origin+pcbnew.VECTOR2I_MM(led_pos+2.5, (Rowheight/2)+(yoffset-2)),
                origin+pcbnew.VECTOR2I_MM(NEXTled_pos-2.5, (Rowheight/2)+(yoffset-2)),
                origin+pcbnew.VECTOR2I_MM(NEXTled_pos-2.5, 2+yoffset)
            )
        s = pcbnew.PCB_SHAPE()
        s.SetShape(pcbnew.SHAPE_T_POLY)
        s.SetPolyPoints(UpperPoints)
        s.SetLayer(edge_cutout_layer)
        board.Add(s)

        LowerPoints = (
                origin+pcbnew.VECTOR2I_MM(led_pos+2.5, (Rowheight/2)+2+yoffset),
                origin+pcbnew.VECTOR2I_MM(led_pos+2.5, Rowheight+(yoffset-2)),
                origin+pcbnew.VECTOR2I_MM(NEXTled_pos-2.5, Rowheight+(yoffset-2)),
                origin+pcbnew.VECTOR2I_MM(NEXTled_pos-2.5, (Rowheight/2)+2+yoffset)
            )
        s = pcbnew.PCB_SHAPE()
        s.SetShape(pcbnew.SHAPE_T_POLY)
        s.SetPolyPoints(LowerPoints)
        s.SetLayer(edge_cutout_layer)
        board.Add(s)

    def Draw_fret_helpers(self, board, origin, fret_length, startHeight, AlphaAngle, Fret_layer, x):
        Fretyoffset = math.tan(AlphaAngle)*fret_length
        Fretheight = startHeight - Fretyoffset*2
        fret = pcbnew.PCB_SHAPE(board, pcbnew.PCB_SHAPE_T, pcbnew.SHAPE_T_SEGMENT)
        fret.SetStart(origin+pcbnew.VECTOR2I_MM(x, +Fretyoffset))
        fret.SetEnd(origin+pcbnew.VECTOR2I_MM(x, Fretheight+Fretyoffset))
        fret.SetWidth(int(0.25 * self.SCALE))
        fret.SetLayer(Fret_layer)
        board.Add(fret)

    def Pcb_Border_cutout(self, board, board1Point, board2Point, board3Point, board4Point, edge_cutout_layer):
        EdgePoints = (
            board1Point+pcbnew.VECTOR2I_MM(7, 1),
            board2Point+pcbnew.VECTOR2I_MM(-7, 0.75),
            board3Point+pcbnew.VECTOR2I_MM(-7, -0.75),
            board4Point+pcbnew.VECTOR2I_MM(7, -1)
        )
        s = pcbnew.PCB_SHAPE()
        s.SetShape(pcbnew.SHAPE_T_POLY)
        s.SetPolyPoints(EdgePoints)
        s.SetLayer(edge_cutout_layer)
        board.Add(s)

    def Fret_Border_draw(self, board, board1Point, board2Point, board3Point, board4Point, dim_layer):
        EdgePoints = (
            board1Point,
            board2Point,
            board3Point,
            board4Point
        )
        s = pcbnew.PCB_SHAPE()
        s.SetShape(pcbnew.SHAPE_T_POLY)
        s.SetPolyPoints(EdgePoints)
        s.SetLayer(dim_layer)
        board.Add(s)

    @staticmethod
    def __pcbpoint(p,origin=pcbnew.wxPoint(0,0)):
        return origin+pcbnew.VECTOR2I_MM(float(p[0]), float(p[1]))


SimplePlugin().register() # Instantiate and register to Pcbnew